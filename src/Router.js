import React, { Component } from 'react';
import { Scene, Router } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { logoutUser } from './actions';
import LoginForm from './components/LoginForm';
import MainScreen from './components/MainScreen';
import StudentProfile from './components/StudentProfile';
import CreateStudent from './components/CreateStudent';
import CreateClinic from './components/CreateClinic';
import SignUpForm from './components/SignUpForm';

class RouterComponent extends Component {
	render() {
	return (
		<Router>
			<Scene
				key="root"
				hideNavBar={true}
				navigationBarStyle={{ backgroundColor: '#7c4471' }}
				titleStyle={{ color: 'white' }}
			>
				<Scene key="auth">
					<Scene
						key="login"
						component={LoginForm}
						title="Login"
					/>
					<Scene
						key="signUp"
						component={SignUpForm}
						title="Register"
					/>
				</Scene>
				<Scene key="main">
					<Scene 
						key="index" 
						component={MainScreen} 
						title="Welcome" 
						onRight={() => this.props.logoutUser()}
						rightButtonImage={require('./assets/logout_icon.png')}
					/>
					<Scene
						key="student" 
						component={StudentProfile} 
						title="Student Profile" 
					/>
					<Scene 
						key="createClinic" 
						component={CreateClinic} 
						title="Clinic Form" 
					/>
					<Scene 
						key="createStudent" 
						component={CreateStudent} 
						title="Student Form" 
					/>
				</Scene>
			</Scene>
		</Router>
	);
	}
}

export default connect(null, { logoutUser })(RouterComponent);
