import SInfo from 'react-native-sensitive-info';

export const USER = 'user';
export const setItem = (key, value) => SInfo.setItem(key, JSON.stringify(value), {});
export const getItem = (key, callback) => SInfo.getItem(key, {}).then(value => callback(JSON.parse(value)));
