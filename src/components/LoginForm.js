import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Card, CardSection, Input, Button, Spinner } from './common';
import { emailChanged, passwordChanged, loginUser } from '../actions';
import { USER, getItem } from '../utils/SharedPrefs';
import SInfo from 'react-native-sensitive-info';

class LoginForm extends Component {
	componentWillMount() {
		SInfo.getItem(USER, {}).then(user => {
			if (user !== null && user !== undefined) Actions.main({ type: 'reset' });
		});
	}

	onEmailChange(text) {
		this.props.emailChanged(text);
	}

	onPasswordChange(text) {
		this.props.passwordChanged(text);
	}

	onButtonClicked() {
		const { password, email, loginUser } = this.props;
		loginUser({ email, password });
	}

	signUp() {
		Actions.signUp();
  }

	renderButton() {
		if (this.props.loading) return <Spinner />;
		return <Button onPress={this.onButtonClicked.bind(this)}>Login</Button>;
	}

	render() {
		const { email, password } = this.props;
		return (
			<Card>
				<CardSection>
					<Input 
					label='Email'
					placeholder='Email'
					onChangeText={this.onEmailChange.bind(this)} 
					value={email}
					/>
				</CardSection>
				<CardSection>
					<Input 
					secureTextEntry label='Password' 
					placeholder='Password'
					onChangeText={this.onPasswordChange.bind(this)}
					value={password}
					/>
				</CardSection>
				<CardSection>
					{this.renderButton()}
				</CardSection>
				<CardSection>
					<Button onPress={this.signUp.bind(this)}>Register</Button>
				</CardSection>
			</Card>
		);
	}
}

const styles = {
	errorText: {
		fontSize: 20,
		alignSelf: 'center',
		color: 'red'
	}
};

const mapStateToProps = ({ auth }) => {
	const { email, password, error, loading } = auth;
	return { email, password, error, loading };
};

export default connect(mapStateToProps, { emailChanged, passwordChanged, loginUser })(LoginForm);
