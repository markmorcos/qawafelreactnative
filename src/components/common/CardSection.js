import React from 'react';
import { View } from 'react-native';

const CardSection = (props) => {
	return (
		<View style={[props.style, styles.containerStyles]}>
			{props.children}
		</View>
		);
};

const styles = {
	containerStyles: {
		// borderBottomWidth: 0,
		padding: 5,
		backgroundColor: 'white',
		justifyContent: 'flex-start',
		flexDirection: 'row',
		// borderColor: '#ddd',
		// position: 'relative'
	}
};

export { CardSection };

