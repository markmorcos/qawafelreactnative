import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, children }) => {
	const { buttonStyle, textStyle } = style;
return (
	<TouchableOpacity style={buttonStyle} onPress={onPress}>
	<Text style={textStyle}>{children}</Text>
	</TouchableOpacity>
	);
};

const style = {
	buttonStyle: {
		flex: 1,
		alignSelf: 'stretch',
		backgroundColor: '#7c4471',
		// marginLeft: 5,
		// marginRight: 5 
	},
	textStyle: {
		alignSelf: 'center',
		color: 'white',
		fontSize: 16,
		fontWeight: '600',
		paddingTop: 10,
		paddingBottom: 10
	}
};

export { Button };

