import React, { Component } from 'react';
import { KeyboardAvoidingView, Platform, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { Dropdown } from 'react-native-material-dropdown';
import { Card, CardSection, Input, Button, Spinner } from './common';
import { getTitles, getSpeciality, signupUser } from '../actions';

const { OS } = Platform;
class SignUpForm extends Component {
	state = {
		title: '',
		firstName: '',
		lastName: '',
		email: '',
		password: '',
		speciality: '',
		mobile: ''
	};

	componentWillMount() {
		this.props.getTitles();
		this.props.getSpeciality();
	}

	onButtonClicked() {
		const title = this.state.title && this.props.titles[this.props.titles.map(title => title.name).indexOf(this.state.title)]._id || '';
		const speciality = this.state.speciality && this.props.specialities[this.props.specialities.map(speciality => speciality.name).indexOf(this.state.speciality)]._id || '';
		const { firstName, lastName, email, password, mobile } = this.state;
		this.props.signupUser(title, firstName, lastName, speciality, email, password, mobile);
	}

	renderButton() {
		if (this.props.loading) return <Spinner />;
		return <Button onPress={this.onButtonClicked.bind(this)}>Submit</Button>;
	}

	render() {
		return (
			<KeyboardAvoidingView behavior={OS === 'ios' ? "padding" : null}>
				<ScrollView>
					<Card style={{ justifyContent: 'center' }}>
						<CardSection>
							<Dropdown
								data={this.props.titles.map(title => {
									return { value: title.name };
								})}
								containerStyle={{ flex: 1 }}
								label='Title'
								value={this.state.title}
								onChangeText={title => 
									{ if (title === 'Admin' || title === 'Nurse') {
										this.setState({ title, speciality: title });
										} else {
											this.setState({ title });
										}
									}
								}
							/>
						</CardSection>
						<CardSection>
							<Input 
								label='First Name'
								placeholder='First Name'
								value={this.state.firstName}
								onChangeText={firstName => this.setState({ firstName })}
							/>
						</CardSection>
						<CardSection>
							<Input 
								label='Last Name'
								placeholder='Last Name'
								value={this.state.lastName}
								onChangeText={lastName => this.setState({ lastName })}
							/>
						</CardSection>
						<CardSection>
							<Dropdown
								data={this.props.specialities.map(refer => {
									return { value: refer.name };
								})}
								containerStyle={{ flex: 1 }}
								label='Speciality'
								value={this.state.speciality}
								onChangeText={speciality => this.setState({ speciality })}
							/>
						</CardSection>
						<CardSection>
							<Input 
								label='Email'
								placeholder='Email'
								onChangeText={email => this.setState({ email })}
							/>
						</CardSection>
						<CardSection>
							<Input 
								label='Password'
								placeholder='Password'
								value={this.state.password}
								onChangeText={password => this.setState({ password })}
							/>
						</CardSection>
						<CardSection>
							<Input 
								label='Mobile'
								placeholder='Mobile'
								value={this.state.mobile}
								onChangeText={mobile => this.setState({ mobile })}
							/>
						</CardSection>
						<CardSection style={{ marginBottom: OS === 'ios' ? 100 : 0 }}>
							{this.renderButton()}
						</CardSection>
					</Card>
				</ScrollView>
			</KeyboardAvoidingView>
		);
	}
}

const mapStateToProps = ({ clinic, auth }) => {
	const { specialities } = clinic;
	const { titles, loading } = auth;
	return { titles, specialities, loading };
};

export default connect(mapStateToProps, { getTitles, getSpeciality, signupUser })(SignUpForm);
