import React, { Component } from 'react';
import { View, ScrollView, Text, ListView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { getPrimaryClinics, getFinalClinics } from '../actions';
import { Card, CardSection, Button } from './common';
import SInfo from 'react-native-sensitive-info';
import { USER } from '../utils/SharedPrefs';

class StudentProfile extends Component {
  componentWillMount() {
    this.props.getPrimaryClinics(this.props.student._id);
    this.props.getFinalClinics(this.props.student._id);
  }

  editProfile() {
    const { student } = this.props;
    Actions.createStudent({ student });
  }

  createClinic() {
    const { student } = this.props;
    Actions.createClinic({ student });
  }

  renderPrimaryRow(clinic) {
    const { _id } = this.props.user;
    const ClinicView = clinic.doctor._id == _id ? TouchableOpacity : View;
    const clinicPress = clinic.doctor._id == _id ? () => Actions.createClinic({ clinic }) : () => {};
    return (
      <ClinicView style={styles.clinicStyle} onPress={clinicPress}>
        <Text style={styles.clinicTextStyle}>{clinic.speciality.name}</Text>
        <Text style={styles.clinicDescriptionStyle}>{clinic.doctor.firstName + ' ' + clinic.doctor.lastName}</Text>
        <Text style={styles.clinicDescriptionStyle}>{clinic.referralReason}</Text>
      </ClinicView>
    );
  }

  renderFinalRow(clinic) {
    const { _id } = this.props.user;
    const ClinicView = clinic.doctor._id == _id ? TouchableOpacity : View;
    const clinicPress = clinic.doctor._id == _id ? () => Actions.createClinic({ clinic }) : () => {};
    return (
      <ClinicView style={styles.clinicStyle} onPress={clinicPress}>
        <Text style={styles.clinicTextStyle}>{clinic.speciality.name}</Text>
        <Text style={styles.clinicDescriptionStyle}>{clinic.doctor.firstName + ' ' + clinic.doctor.lastName}</Text>
        <Text style={styles.clinicDescriptionStyle}>{clinic.finalDiagnosis}</Text>
      </ClinicView>
    );
  }

  render() {
    const { student } = this.props;
    const { _id, firstName, lastName, studentId, birthdate, mobile, landline, city, school, knownMedicalConditions } = student;
    const prs = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.primaryDataSource = prs.cloneWithRows(this.props.primaryClinics);
    const frs = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.finalDataSource = frs.cloneWithRows(this.props.finalClinics);
    const { container, titleStyle, labelStyle } = styles;
    const age = new Date().getFullYear() - Number((birthdate.indexOf('/') !== -1 ? birthdate.split('/').reverse().join('-') : birthdate).split('-')[0]);
    return (
      <ScrollView>
        <Card style={container}>
          <CardSection>
            <Text style={titleStyle}>Details</Text>
          </CardSection>
          <CardSection>
            <Text><Text style={labelStyle}>ID</Text>: {studentId}</Text>
          </CardSection>
          <CardSection>
            <Text><Text style={labelStyle}>Name</Text>: {firstName + ' ' + lastName}</Text>
          </CardSection>
          <CardSection>
            <Text><Text style={labelStyle}>Birthdate</Text>: {birthdate}</Text>
          </CardSection>
          <CardSection>
            <Text><Text style={labelStyle}>Age</Text>: {age}</Text>
          </CardSection>
          <CardSection>
            <Text><Text style={labelStyle}>Mobile</Text>: {mobile}</Text>
          </CardSection>
          <CardSection>
            <Text><Text style={labelStyle}>Landline</Text>: {landline}</Text>
          </CardSection>
          <CardSection>
            <Text><Text style={labelStyle}>City</Text>: {city.name}</Text>
          </CardSection>
          <CardSection>
            <Text><Text style={labelStyle}>School</Text>: {school.name}</Text>
          </CardSection>
          <CardSection>
            <Text><Text style={labelStyle}>Known Medical Conditions</Text>: {knownMedicalConditions}</Text>
          </CardSection>
          <CardSection>
            <Button onPress={this.editProfile.bind(this)}>Edit Profile</Button>
          </CardSection>
          <CardSection>
            <Button onPress={this.createClinic.bind(this)}>Open Clinic</Button>
          </CardSection>
        </Card>
        {this.props.primaryClinics.length ? <Card>
          <CardSection>
            <Text style={titleStyle}>Referrals</Text>
          </CardSection>
          <CardSection>
            <ListView enableEmptySections dataSource={this.primaryDataSource} renderRow={this.renderPrimaryRow.bind(this)} />
          </CardSection>
        </Card> : <View />}
        {this.props.finalClinics.length ? <Card>
          <CardSection>
            <Text style={titleStyle}>Clinics</Text>
          </CardSection>
           <CardSection>
          <ListView enableEmptySections dataSource={this.finalDataSource} renderRow={this.renderFinalRow.bind(this)} />
          </CardSection>
        </Card> : <View />}
      </ScrollView>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'row'
  },
  titleStyle: {
    fontWeight: 'bold',
    fontSize: 20
  },
  labelStyle: {
    fontWeight: 'bold',
    padding: 5
  },
  clinicStyle: {
    backgroundColor: '#7c4471',
    padding: 5,
    marginTop: 5
  },
  clinicTextStyle: {
    color: 'white',
    fontWeight: 'bold'
  },
  clinicDescriptionStyle: {
    color: 'white'
  }
};

const mapStateToProps = ({ clinic }) => {
  const { primaryClinics, finalClinics } = clinic;
  return { primaryClinics, finalClinics };
};
export default connect(mapStateToProps, { getPrimaryClinics, getFinalClinics })(StudentProfile);
