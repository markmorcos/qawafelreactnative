import React, { Component } from 'react';
import { NetInfo } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-simple-toast';
import { Card, CardSection, Button, Input, Spinner } from './common';
import { getAll, postAll, getCities, getSchools, readStudent } from '../actions';
import { Dropdown } from 'react-native-material-dropdown';
import SInfo from 'react-native-sensitive-info';

class MainScreen extends Component {
  state = {
    city: '',
    school: '',
    studentId: '',
    chosen: false
  }

  componentWillMount() {
    const { getAll, postAll, schools, getCities, getSchools } = this.props;
    NetInfo.addEventListener('connectionChange', ({ type }) => {
      const { getAll, cities, schools } = this.props;
      const { school } = this.state;
      if (type !== 'none' && type !== 'unknown') {
        if (cities.length && city && schools.length && school) {
          getAll(
            cities[cities.map(city => city.name).indexOf(city)]._id,
            schools[schools.map(school => school.name).indexOf(school)]._id
          );
        } else if (cities.length && city) {
          // getAll(cities[cities.map(city => city.name).indexOf(city)]._id, '');
        } else if (schools.length && school) {
          getAll('', schools[schools.map(school => school.name).indexOf(school)]._id);          
        }
        postAll();
      }
    });
    getCities();
    getSchools();
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.cities.length && this.state.city !== nextState.city) {
      if (nextProps.schools.length && nextState.school) {
        this.props.getAll(
          nextProps.cities[nextProps.cities.map(city => city.name).indexOf(nextState.city)]._id,
          nextProps.schools[nextProps.schools.map(school => school.name).indexOf(nextState.school)]._id
        );
      } else {
        // this.props.getAll(nextProps.cities[nextProps.cities.map(city => city.name).indexOf(nextState.city)]._id, '');
      }
    }
    if (nextProps.schools.length && this.state.school !== nextState.school) {
      if (nextProps.cities.length && nextState.city) {
        this.props.getAll(
          nextProps.cities[nextProps.cities.map(city => city.name).indexOf(nextState.city)]._id,
          nextProps.schools[nextProps.schools.map(school => school.name).indexOf(nextState.school)]._id
        );
      } else {
        this.props.getAll('', nextProps.schools[nextProps.schools.map(school => school.name).indexOf(nextState.school)]._id);
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.cities.length && nextProps.schools.length) {
      SInfo.getItem('city', {}).then(city => {
        SInfo.getItem('school', {}).then(school => {
          if (school !== null && school !== undefined) {
            this.setState({ school: nextProps.schools[nextProps.schools.map(school => school.name).indexOf(school)].name });
          }
          if (city !== null && city !== undefined) {
            this.setState({ city: nextProps.cities[nextProps.cities.map(city => city.name).indexOf(city)].name });
          }
        });
      });
    }
  }

  scanStudent() {
    if (this.state.chosen) {
      this.props.readStudent(
        this.state.city && this.props.cities[this.props.cities.map(city => city.name).indexOf(this.state.city)]._id || '',
        this.state.school && this.props.schools[this.props.schools.map(school => school.name).indexOf(this.state.school)]._id || '',
        this.state.studentId
      );
    } else if (this.state.city && this.state.school) {
      this.setState({ chosen: true });
    }
  }

  createStudent() {
    Actions.createStudent();
  }
     
  render() {
    const { cities, schools, loading } = this.props;
    if (loading) return <Spinner />;
    const { city, school, chosen } = this.state;
    return (
      <Card style={styles.container}>
        {!chosen && <CardSection>
          <Dropdown
            data={cities.map(city => {
              return { value: city.name };
            })}
            containerStyle={{ flex: 1 }}
            label='City'
            value={city}
            onChangeText={city => {
              SInfo.setItem('city', city, {});
              this.setState({ city, school: '' });
            }}
          />
        </CardSection>}
        {!chosen && <CardSection>
          <Dropdown
            data={schools.filter(school => {
              if (!city) return false;
              return school.city == cities[cities.map(city => city.name).indexOf(city)]._id;
            }).map(school => {
              return { value: school.name };
            })}
            containerStyle={{ flex: 1 }}
            label='School'
            value={school}
            onChangeText={school => {
              SInfo.setItem('school', school, {});
              this.setState({ school });
            }}
          />
        </CardSection>}
        {chosen && <CardSection><Input label='Student ID' placeholder='Student ID' onChangeText={studentId => this.setState({ studentId })} /></CardSection>}
        <CardSection><Button onPress={this.scanStudent.bind(this)}>{chosen ? 'Search' : 'Get'}</Button></CardSection>
        {chosen && <CardSection><Button onPress={this.createStudent.bind(this)}>Add new student</Button></CardSection>}
        {chosen && <CardSection><Button onPress={() => this.setState({ chosen: false })}>Back</Button></CardSection>}
      </Card>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'row'
  }
};

const mapStateToProps = ({ auth, main }) => {
  const { loading } = auth;
  const { cities, schools } = main;
  return { cities, schools, loading };
};

export default connect(mapStateToProps, { getAll, postAll, getCities, getSchools, readStudent })(MainScreen);
