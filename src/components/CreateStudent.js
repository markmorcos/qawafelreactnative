import React, { Component } from 'react';
import { connect } from 'react-redux';
import { KeyboardAvoidingView, Platform, ScrollView } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { Dropdown } from 'react-native-material-dropdown';
import { Card, CardSection, Input, Button } from './common';
import { getCities, getSchools, createStudent } from '../actions';

const { OS } = Platform;

class CreateStudent extends Component {
	state = {
		studentId: '',
		firstName: '',
		lastName: '',
		birthdate: '1/1/2000',
		mobile: '',
		landline: '',
		city: '',
		school: '',
		knownMedicalConditions: ''
	};
	
	componentWillMount() {
		const { student, getCities, getSchools } = this.props;
		if (student) {
			var { studentId, firstName, lastName, birthdate, mobile, landline, city, school, knownMedicalConditions } = student;
			city = city.name;
			school = school.name;
			this.setState({ studentId, firstName, lastName, birthdate, mobile, landline, city, school, knownMedicalConditions });
		}
		getCities();
		getSchools();
	}

	onButtonClicked() {
		var { studentId, firstName, lastName, birthdate, mobile, landline, city, school, knownMedicalConditions } = this.state;
		const { student, cities, schools, createStudent } = this.props;
		city = city && cities[cities.map(city => city.name).indexOf(city)] || '';
		school = city && schools[schools.map(school => school.name).indexOf(school)] || '';
		const action = student ? 'edit' : 'add';
		const sid = student ? student._id : null;
    createStudent(action, sid, studentId, firstName, lastName, birthdate, mobile, landline, city, school, knownMedicalConditions);
	}

	render() {
		const { studentId, firstName, lastName, birthdate, mobile, landline, city, school, knownMedicalConditions } = this.state;
		const { cities, schools } = this.props;
		return (
			<KeyboardAvoidingView behavior={OS === 'ios' ? "padding" : null} >
				<ScrollView>
					<Card>
						<CardSection>
							<Input 
								label='Code'
								placeholder='Code'
								onChangeText={studentId => this.setState({ studentId })}
								value={studentId}
							/>
						</CardSection>
						<CardSection>
							<Input 
								label='First Name'
								placeholder='First Name'
								onChangeText={firstName => this.setState({ firstName })}
								value={firstName}
							/>
						</CardSection>
						<CardSection>
							<Input 
								label='Last Name'
								placeholder='Last Name'
								onChangeText={lastName => this.setState({ lastName })}
								value={lastName}
							/>
						</CardSection>
						<CardSection>
							<DatePicker
				        date={birthdate.indexOf('/') !== -1 ? birthdate.split('/').reverse().join('-') : birthdate}
				        mode="date"
				        style={{ flex: 1 }}
				        placeholder="Birthdate"
				        format="YYYY-MM-DD"
				        confirmBtnText="Confirm"
				        cancelBtnText="Cancel"
				        customStyles={{
				          dateIcon: {
				            position: 'absolute',
				            left: 0,
				            top: 4,
				            marginLeft: 0
				          },
				          dateInput: {
				            marginLeft: 36
				          }
				        }}
				        onDateChange={birthdate => this.setState({ birthdate })}
				      />
						</CardSection>
						<CardSection>
							<Input 
								label='Mobile'
								placeholder='Mobile'
								onChangeText={mobile => this.setState({ mobile })}
								value={mobile}
							/>
						</CardSection>
						<CardSection>
							<Input 
								label='Landline'
								placeholder='Landline'
								onChangeText={landline => this.setState({ landline })}
								value={landline}
							/>
						</CardSection>
						<CardSection>
							<Dropdown
								data={cities.map(city => {
									return { value: city.name };
								})}
								containerStyle={{ flex: 1 }}
					      label='City'
			        	onChangeText={city => {
			        		this.setState({ city, school: '' });
			        	}}
			        	value={city}
			        />
			       </CardSection>
						<CardSection>
							<Dropdown
								data={schools.filter(school => {
		              if (!city) return false;
		              return school.city == cities[cities.map(city => city.name).indexOf(city)]._id;
		            }).map(school => {
									return { value: school.name };
								})}
								containerStyle={{ flex: 1 }}
					      label='School'
			        	onChangeText={school => this.setState({ school })}
			        	value={school}
			        />
			       </CardSection>
						<CardSection>
							<Input 
								label='Known Medical Conditions'
								placeholder='Known Medical Conditions'
								onChangeText={knownMedicalConditions => this.setState({ knownMedicalConditions })}
								value={knownMedicalConditions}
							/>
						</CardSection>
						<CardSection style={{ marginBottom: OS === 'ios' ? 80 : 0 }}>
							<Button onPress={this.onButtonClicked.bind(this)}>Submit</Button>
						</CardSection>
					</Card>
				</ScrollView>
			</KeyboardAvoidingView>
		);
	}
}

const mapStateToProps = ({ main }) => {
	const { cities, schools } = main;
	return { cities, schools };
};

export default connect(mapStateToProps, {
	getCities,
	getSchools,
	createStudent
})(CreateStudent);
