import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-simple-toast';
import SInfo from 'react-native-sensitive-info';
import {
	View,
	TextInput, 
	ScrollView, 
	TouchableOpacity, 
	Image, 
	Text,
	KeyboardAvoidingView,
	Platform,
	Dimensions
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { Dropdown } from 'react-native-material-dropdown';
import { Spinner, Card, CardSection, Button } from './common';
import { propChanged, createClinic, getSpeciality } from '../actions';
import { USER } from '../utils/SharedPrefs';
import { IMG_URL } from '../utils/data';
const { OS } = Platform;

class CreateClinic extends Component {
	componentWillMount() {
		const { clinic } = this.props;
		this.props.getSpeciality();
		if (clinic) {
			var {
				action,
				speciality,
				doctor, 
				student, 
				complaint, 
				history, 
				findings, 
				primaryDiagnosis, 
				referral, 
				referralReason,
				finalDiagnosis, 
				attachments
			} = clinic;
			speciality = speciality.name;
			referral = referral && referral.name || '';
			doctor = doctor._id;
			this.onPropChange('speciality', speciality); 
			this.onPropChange('doctor', doctor); 
			this.onPropChange('student', student); 
			this.onPropChange('complaint', complaint); 
			this.onPropChange('history', history); 
			this.onPropChange('findings', findings); 
			this.onPropChange('primaryDiagnosis', primaryDiagnosis); 
			this.onPropChange('referral', referral);
			this.onPropChange('referralReason', referralReason);
			this.onPropChange('finalDiagnosis', finalDiagnosis);
			this.onPropChange('attachments', attachments.filter(attachment => attachment.data.indexOf('uploads') === -1));
			this.onPropChange('originalAttachments', attachments.filter(attachment => attachment.data.indexOf('uploads') !== -1));
		}
	}

	onPhotoPress() {
    ImagePicker.showImagePicker({ title: 'Add an attachment', quality: 0.5 }, response => {
      if (!response.didCancel) {
        this.onPropChange('attachments', [...this.props.attachments.concat([{ caption: '', data: response }])]);
      }
    });
  }
  
  onButtonPress() {
		SInfo.getItem(USER, {}).then(value => {
			const user = JSON.parse(value); 
			const {
				navigation,
				createClinic,
				specialities,
				clinic,
			} = this.props;
			const {
				speciality,
				complaint,
				history,
				findings,
				primaryDiagnosis,
				referral,
				referralReason,
				finalDiagnosis,
				attachments,
				student
			} = this.props;
			const action = clinic && clinic.action ? clinic.action : (clinic ? 'edit' : 'add');	
			if (clinic) {
				if (user._id === clinic.doctor._id) {
					createClinic(
						action,
						student,
						speciality ? specialities[specialities.map(speciality => speciality.name).indexOf(speciality)] : '',
						complaint,
						history,
						findings,
						primaryDiagnosis,
						referral ? specialities[specialities.map(referral => referral.name).indexOf(referral)] : '',
						referralReason,
						finalDiagnosis,
						attachments,
						clinic._id
					);
				} else {
					Actions.pop();
					Toast.show('You are not allowed to edit this clinic');
				}
			} else {
				const { student } = navigation.state.params;
	    	createClinic(
					action,
					student,
					speciality ? specialities[specialities.map(speciality => speciality.name).indexOf(speciality)] : '',
					complaint,
					history,
					findings,
					primaryDiagnosis,
					referral ? specialities[specialities.map(referral => referral.name).indexOf(referral)] : '',
					referralReason,
					finalDiagnosis,
					attachments
				);
			}
		});
  }

  renderOriginalAttachments() {
    const { addPhotoStyle, iconStyle, photoStyle, textStyle, inputStyle } = styles;
    return this.props.originalAttachments.map((attachment, index) => {
    	return (
	      <View key={index} style={[addPhotoStyle, photoStyle, { backgroundColor: 'white' }]}>
      		<Image source={{ uri: IMG_URL + attachment.data }} style={photoStyle} resizeMode="contain" />
      		<Text>{attachment.caption}</Text>
	      </View>
	    );
    });
  }

  renderAttachments() {
    const { addPhotoStyle, iconStyle, photoStyle, textStyle } = styles;
    return this.props.attachments.map((attachment, index) => {
    	return (
    		<View key={index}>
		      <TouchableOpacity
		      	style={[addPhotoStyle, photoStyle, { backgroundColor: 'white' }]}
		      	onLongPress={() => {
		      		this.props.attachments.splice(index, 1);
		      		this.forceUpdate();
		      	}}
		      >
		        <Image style={photoStyle} source={attachment.data} resizeMode="contain" />
		      </TouchableOpacity>
	    		<TextInput
						value={this.props.attachments[index].caption}
						onChangeText={caption => {
							const { attachments } = this.props;
							const newAttachments = [...attachments];
							newAttachments[index].caption = caption;
							this.onPropChange('attachments', newAttachments);
						}}
		    		placeholder='Caption' 
		    		style={styles.inputStyle}
	    		/> 
	      </View>
	    );
    });
  }

  renderPhoto() {
    const { addPhotoStyle, iconStyle, photoStyle, textStyle } = styles;
    return (
    	<View style={{ width: '100%', flex: 1, display: 'flex', flexDirection: 'column' }}>
	    	{this.renderOriginalAttachments()}
	    	{this.renderAttachments()}
	      <TouchableOpacity
	        onPress={this.onPhotoPress.bind(this)}
	        style={[addPhotoStyle, photoStyle, { height: 100 }]}
	      >
	        <Image style={iconStyle} source={require('../assets/add_photo.png')} />
	        <Text style={textStyle}>Add an attachment</Text>
	      </TouchableOpacity>
	    </View>
    );
  }

   onPropChange(key, value) {
    this.props.propChanged(key, value);
  }

   renderButton() {
    if (this.props.loading) return <Spinner />;
    return (<Button onPress={this.onButtonPress.bind(this)}>Save</Button>);
  }
	render() {
		const { inputStyle } = styles;
		const { specialities } = this.props;
		return (
			<KeyboardAvoidingView behavior={OS === 'ios' ? "padding" : null} >
				<ScrollView>
					<Card>
						<CardSection>
							<Dropdown
								data={specialities.map(refer => {
									return { value: refer.name };
								})}
								containerStyle={{ flex: 1 }}
								label='Speciality'
								value={this.props['speciality']}
								onChangeText={speciality => this.onPropChange('speciality', speciality)}
							/>
						</CardSection>
						<CardSection>
							<TextInput 
								value={this.props['complaint']}
								onChangeText={complaint => this.onPropChange('complaint', complaint)}
								placeholder='Complaint'
								style={inputStyle}
							/>
						</CardSection>
						<CardSection>
							<TextInput
								value={this.props['history']}
								onChangeText={history => this.onPropChange('history', history)}
								placeholder='History of present illness'
								style={inputStyle}
							/>
						</CardSection>
						<CardSection>
							<TextInput
								value={this.props['findings']}
								onChangeText={findings => this.onPropChange('findings', findings)}
								placeholder='Basic medical findings'
								style={inputStyle}
							/>
						</CardSection>
						<CardSection>
							<TextInput
								value={this.props['primaryDiagnosis']}
								onChangeText={primaryDiagnosis => this.onPropChange('primaryDiagnosis', primaryDiagnosis)}
								placeholder='Primary diagnosis'
								style={inputStyle}
							/>
						</CardSection>
						<CardSection>
							<TextInput
								value={this.props['finalDiagnosis']}
								onChangeText={finalDiagnosis => this.onPropChange('finalDiagnosis', finalDiagnosis)}
								placeholder='Procedures & final diagnosis'
								style={inputStyle}
							/>
						</CardSection>
						<CardSection>
							<Dropdown
								data={[{ name: '' }].concat(specialities).map(special => {
									return { value: special.name };
								})}
								containerStyle={{ flex: 1 }}
								label='Referral'
								value={this.props['referral']}
								onChangeText={referral => {
									if (!referral) {
										this.onPropChange('referralReason', referralReason);
									}
									this.onPropChange('referral', referral);
								}}
							/>
						</CardSection>
						<CardSection>
							<TextInput
								value={this.props['referralReason']}
								onChangeText={referralReason => this.onPropChange('referralReason', referralReason)}
								placeholder='Referral reason'
								style={inputStyle}
								editable={this.props.referral !== ''}
							/>
						</CardSection>
						<CardSection>	
							{this.renderPhoto()}
						</CardSection>
						<CardSection style={{ marginBottom: OS === 'ios' ? 80 : 0 }}>
							{this.renderButton()}
						</CardSection>
					</Card>
				</ScrollView>
			</KeyboardAvoidingView>
		);
	}
}
const styles = {
  addPhotoStyle: {
    backgroundColor: '#242424',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    flex: 1
  },
  photoStyle: {
  	flex: 1,
    width: '100%',
    height: 500
  },
  iconStyle: {
    width: 25,
    height: 25
  },
  textStyle: {
    color: 'white',
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: 10
  },
	inputStyle: {
		color: '#000',
		marginTop: 10,
		marginBottom: 10,
		paddingRight: 5,
		paddingLeft: 5,
		fontSize: 18,
		lineHeight: 23,
		flex: 1
	}
};
const mapStateToProps = ({ clinic }) => {
  const {
		speciality,
		doctor,
		student,
		complaint,
		history,
		findings,
		primaryDiagnosis,
		referral,
		referralReason,
		finalDiagnosis,
		attachments,
		originalAttachments,
		loading,
		error,
		specialities
  } = clinic;
  return {
		speciality,
		doctor,
		student,
		complaint,
		history,
		findings,
		primaryDiagnosis,
		referral,
		referralReason,
		finalDiagnosis,
		attachments,
		originalAttachments,
		error, 
		loading,
		specialities
	};
};

export default connect(mapStateToProps, {
	propChanged, 
	createClinic, 
	getSpeciality
})(CreateClinic);
