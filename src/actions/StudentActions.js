import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import SInfo from 'react-native-sensitive-info';
import { Keyboard, NetInfo } from 'react-native';
import Toast from 'react-native-simple-toast';
import {
	ALL_CITIES_URL,
	ALL_SCHOOLS_URL,
	CREATE_STUDENT_URL,
	UPDATE_STUDENT_URL,
	READ_STUDENT_URL
} from '../utils/data';
import {
	GET_ALL_CITIES,
	GET_ALL_SCHOOLS,
	GET_FAILED,
	CREATE_STUDENT,
	READ_STUDENT
} from './types';
import { USER, getItem } from '../utils/SharedPrefs';

export const getCitiesCall = () => {
	return dispatch => {
    NetInfo.isConnected.fetch().then(isConnected => {
      SInfo.getItem('cities', {}).then(value => {
        if (!isConnected && value !== null && value !== undefined) {
          return dispatch({ type: GET_ALL_CITIES, payload: JSON.parse(value) });
        } else {
          axios.get(ALL_CITIES_URL)
          .then(({ data }) => {
            SInfo.setItem('cities', JSON.stringify(data), {});
            dispatch({ type: GET_ALL_CITIES, payload: data });
      		})
          .catch(() => Toast.show('Get cities failed'));
        }
      });
    });
  };
};

export const getSchoolsCall = () => {
	return dispatch => {
    NetInfo.isConnected.fetch().then(isConnected => {
      SInfo.getItem('schools', {}).then(value => {
        if (!isConnected && value !== null && value !== undefined) {
          return dispatch({ type: GET_ALL_SCHOOLS, payload: JSON.parse(value) });
        } else {
          axios.get(ALL_SCHOOLS_URL)
          .then(({ data }) => {
            SInfo.setItem('schools', JSON.stringify(data), {});
            dispatch({ type: GET_ALL_SCHOOLS, payload: data });
      		})
          .catch(() => Toast.show('Get schools failed'));
        }
      });
    });
  };
};

export const createStudentCall = (action, sid, studentId, firstName, lastName, birthdate, mobile, landline, city, school, knownMedicalConditions) => {
	return dispatch => {
		dispatch({ type: CREATE_STUDENT });
    NetInfo.isConnected.fetch().then(isConnected => {
	    SInfo.getItem(USER, {}).then(value => {
	    	const user = JSON.parse(value);
	    	const { token } = user;
	      SInfo.getItem('students', {}).then(value => {
	        const students = value !== null && value !== undefined ? JSON.parse(value) : {};
	        const objectId = ((rnd = r16 => Math.floor(r16).toString(16)) => rnd(Date.now() / 1000) + ' '.repeat(16).replace(/./g, () => rnd(Math.random() * 16)))();
	        const _id = sid || objectId;
	        if (!isConnected) {
	          if (!firstName || !lastName || !birthdate || !mobile || !city || !school) {
	          	Toast.show('Incomplete data provided');
	            return;
	          }
	          students[_id] = {
	            action,
						  doctor: user,
	          	studentId,
	            firstName,
	            lastName,
	            birthdate,
	            mobile,
	            landline,
	            city,
	            school,
	            knownMedicalConditions,
	            _id
	          };
	          SInfo.setItem('students', JSON.stringify(students), {});
						dispatch({ type: CREATE_STUDENT });
						Actions.main({ type: 'reset' });
						Actions.student({ student: students[_id], user });
	        } else {
						axios.post(action === 'edit' ? UPDATE_STUDENT_URL : CREATE_STUDENT_URL, {
						  token,
						  studentId,
						  firstName,
						  lastName,
						  birthdate,
						  mobile,
						  landline,
						  city: city._id,
						  school: school._id,
						  knownMedicalConditions,
						  id: sid
						})
						.then(({ data }) => {
							if (!data.success) {
								Toast.show(data.message);
								dispatch({ type: GET_FAILED });
							} else {
	              SInfo.getItem('students', {}).then(value => {
	                const students = value !== null && value !== undefined ? JSON.parse(value) : {};
	                const { studentId, firstName, lastName, birthdate, mobile, landline, city, school, knownMedicalConditions, _id } = data.user;
	                students[_id] = {
				          	studentId,
				            firstName,
				            lastName,
				            birthdate,
				            mobile,
				            landline,
				            city,
				            school,
				            knownMedicalConditions,
				            _id
	                };
	                SInfo.setItem('students', JSON.stringify(students), {});
									dispatch({ type: CREATE_STUDENT });
									Actions.main({ type: 'reset' });
									Actions.student({ student: data.user, user });
								});
							}
						})
				    .catch(() => Toast.show('Submit student failed'));
	        }
				});
		  });
	  });
	};
};

export const readStudentCall = (city = '', school = '', id = '') => {
	return dispatch => {
    SInfo.getItem(USER, {}).then(value => {
    	const user = JSON.parse(value);
    	const { token } = user;
      NetInfo.isConnected.fetch().then(isConnected => {
        if (!isConnected) {
          SInfo.getItem('students', {}).then(value => {
            const studentsObject = value !== null && value !== undefined ? JSON.parse(value) : {};
            let readStudent = null;
            for (var key in studentsObject) {
              if (studentsObject.hasOwnProperty(key)) {
                const student = studentsObject[key];
                if (student.city._id == city && student.school._id == school && student.studentId == id) {
                	readStudent = student;
                }
              }
            }
						if (readStudent) {
							dispatch({ type: READ_STUDENT, payload: readStudent });
							Actions.student({ student: readStudent, user });
						  Keyboard.dismiss();
						} else {
							Toast.show('Student not found');
						}
          });
        } else {
					axios.get(READ_STUDENT_URL + `?token=${token}&cityId=${city}&schoolId=${school}&studentId=${id}`).then(({ data }) => {
						if (!data.success) {
							Toast.show(data.message);
						} else {
							dispatch({ type: READ_STUDENT, payload: data.user });
							Actions.student({ student: data.user, user });
						  Keyboard.dismiss();
						}
					})
			    .catch(() => Toast.show('Read student failed'));
			  }
		  });
	  });
	};
};
