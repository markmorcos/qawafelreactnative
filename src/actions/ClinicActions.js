import { Actions } from 'react-native-router-flux';
import { Keyboard, NetInfo, Image } from 'react-native';
import Toast from 'react-native-simple-toast';
import SInfo from 'react-native-sensitive-info';
import axios from 'axios';
import {
  CREATE_CLINC_URL,
	ALL_SPECIALITIES_URL, 
	READ_CLINICS_URL,
  UPDATE_CLINIC_URL
} from '../utils/data';
import { 
	CLINIC_PROPS_CHANGED,
	CREATE_CLINIC,
	CREATE_CLINIC_SUCCESS,
	CREATE_CLINIC_FAIL,
	GET_ALL_SPECIALITY,
  GET_PRIMARY_CLINICS,
  GET_FINAL_CLINICS
} from './types';
import { USER, getItem } from '../utils/SharedPrefs';
import { IMG_URL } from '../utils/data';

export const propChangedCall = (key, value) => {
  return { type: CLINIC_PROPS_CHANGED, payload: { key, value } };
};

const createClinicSuccess = (dispatch, student) => {
  SInfo.getItem(USER, {}).then(value => {
    const user = JSON.parse(value);
    dispatch({ type: CREATE_CLINIC_SUCCESS });
    Keyboard.dismiss();
    Actions.main({ type: 'reset' });
    Actions.student({ student, user });
  });
};

const createClinicFail = (dispatch, error) => {
  dispatch({ type: CREATE_CLINIC_FAIL, payload: error });
  Toast.show(error);
};

export const createClinicCall = (action, student, speciality = '', complaint, history, findings, primaryDiagnosis, referral = '', referralReason = '', finalDiagnosis = '', attachments, clinicId = '') => {
  return dispatch => {
    dispatch({ type: CREATE_CLINIC });
    NetInfo.isConnected.fetch().then(isConnected => {
      SInfo.getItem(USER, {}).then(value => {
        const doctor = JSON.parse(value);
        SInfo.getItem('clinics', {}).then(value => {
          const clinics = value !== null && value !== undefined ? JSON.parse(value) : {};
          const objectId = ((rnd = r16 => Math.floor(r16).toString(16)) => rnd(Date.now() / 1000) + ' '.repeat(16).replace(/./g, () => rnd(Math.random() * 16)))();
          const _id = clinicId || objectId;
          if (!isConnected) {
            if (!speciality || !complaint || !history || !findings || !primaryDiagnosis) {
              return createClinicFail(dispatch, 'Incomplete data provided');
            }
            clinics[_id] = {
              action,
              student,
              doctor,
              speciality,
              complaint,
              history,
              findings,
              primaryDiagnosis,
              referral,
              referralReason,
              finalDiagnosis,
              attachments,
              _id
            };
            SInfo.setItem('clinics', JSON.stringify(clinics), {});
            return createClinicSuccess(dispatch, student);
          } else {
            axios.post(action === 'edit' ? UPDATE_CLINIC_URL : CREATE_CLINC_URL, {
              action,
              token: doctor.token,
              doctor: doctor._id,
              student: student._id,
              speciality: speciality._id,
              complaint,
              history,
              findings,
              primaryDiagnosis,
              referral: referral ? referral._id : undefined,
              referralReason,
              finalDiagnosis,
              attachments: JSON.stringify(attachments.map(attachment => {
                return { caption: attachment.caption, data: attachment.data.data };
              })),
              id: clinicId
            })
            .then(response => {
              if (!response.data.success) { 
                return createClinicFail(dispatch, response.data.message); 
              }
              SInfo.getItem('clinics', {}).then(value => {
                const clinics = value !== null && value !== undefined ? JSON.parse(value) : {};
                const { clinic } = response.data;
                clinics[clinic._id] = clinic;
                clinic.attachments.forEach(attachment => Image.prefetch(IMG_URL + attachment.data));
                SInfo.setItem('clinics', JSON.stringify(clinics), {});
                return createClinicSuccess(dispatch, student);
              });
              return createClinicSuccess(dispatch, student);
            })
            .catch(() => createClinicFail(dispatch, 'Submit clinic failed'));
          }
        });
      });
    });
  };
};

export const getSpecialityCall = () => {
	return dispatch => {
    NetInfo.isConnected.fetch().then(isConnected => {
      SInfo.getItem('specialities', {}).then(value => {
        if (!isConnected && value !== null && value !== undefined) {
          return dispatch({ type: GET_ALL_SPECIALITY, payload: JSON.parse(value) });
        } else {
          axios.get(ALL_SPECIALITIES_URL)
          .then(({ data }) => {
            SInfo.setItem('specialities', JSON.stringify(data), {});
            dispatch({ type: GET_ALL_SPECIALITY, payload: data });
      		})
          .catch(() => Toast.show('Get specialities failed'));
        }
      });
    });
  };
};

export const getClinicsCall = (id, type) => {
	return dispatch => {
    SInfo.getItem(USER, {}).then(value => {
      const { token, speciality } = JSON.parse(value);
      SInfo.getItem('clinics', {}).then(value => {
        const clinicsObject = value !== null && value !== undefined ? JSON.parse(value) : {};
        NetInfo.isConnected.fetch().then(isConnected => {
          if (!isConnected) {
            const clinics = [];
            for (var key in clinicsObject) {
              if (clinicsObject.hasOwnProperty(key)) {
                const clinic = clinicsObject[key];
                if (type === 'primary' && clinic.student._id == id && clinic.referral && clinic.referral._id == speciality
                  || type === 'final' && clinic.student._id == id) {
                  clinics.push(clinic);
                }
              }
            }
            dispatch({ type: type === 'primary' ? GET_PRIMARY_CLINICS : GET_FINAL_CLINICS, payload: clinics });
          } else {
            axios.get(READ_CLINICS_URL + `?token=${token}&studentId=${id}&type=${type}`).then(({ data }) => {
              dispatch({ type: type === 'primary' ? GET_PRIMARY_CLINICS : GET_FINAL_CLINICS, payload: data });
              data.forEach(clinic => {
                if (!clinicsObject[clinic._id] || !clinicsObject[clinic._id].action) {
                  clinicsObject[clinic._id] = clinic;
                  SInfo.setItem('clinics', JSON.stringify(clinicsObject), {});
                }
              });
            })
            .catch(() => Toast.show('Get clinics failed'));
          }
        });
      });        
    });
  };
};
