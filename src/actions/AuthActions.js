import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import { Keyboard, Alert, NetInfo, Image } from 'react-native';
import Toast from 'react-native-simple-toast';
import { USER, setItem } from '../utils/SharedPrefs';
import {
  GET_ALL,
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_START,
  LOGINFAIL, 
  LOGIN,
  SIGNUP,
  LOGOUT_USER,
  GET_ALL_TITLES,
  CREATE_CLINIC,
  CREATE_CLINIC_SUCCESS,
  CREATE_CLINIC_FAIL,
} from './types';
import {
  IMG_URL,
  ALL_URL,
  ALL_TITLES_URL,
  LOGIN_URL,
  CREATE_DR_URL,
  CREATE_CLINC_URL,
  UPDATE_CLINIC_URL,
  CREATE_STUDENT_URL,
  UPDATE_STUDENT_URL,
  CREATE_STUDENT
} from '../utils/data';
import SInfo from 'react-native-sensitive-info';

export const getAllCall = (cityId = '', schoolId = '') => {
  return dispatch => {
    dispatch({ type: LOGIN_START });
    axios.get(`${ALL_URL}?city=${cityId}&school=${schoolId}`)
    .then(({ data }) => {
      var done = 0;
      var total = 0;
      SInfo.setItem('cities', JSON.stringify(data.cities), {});
      SInfo.setItem('schools', JSON.stringify(data.schools), {});
      SInfo.setItem('specialities', JSON.stringify(data.specialities), {});
      SInfo.setItem('titles', JSON.stringify(data.titles), {});
      SInfo.getItem('clinics', {}).then(value => {
        SInfo.getItem('students', {}).then(value => {
          const clinics = value !== null && value !== undefined ? JSON.parse(value) : {};
          const students = value !== null && value !== undefined ? JSON.parse(value) : {};
          for (var key in clinics) {
            if (clinics.hasOwnProperty(key)) {
              const student = clinics[key];
              if (!student.action) delete clinics[key];
            }
          }
          for (var key in students) {
            if (students.hasOwnProperty(key)) {
              const student = students[key];
              if (!student.action) delete students[key];
            }
          }
          const newClinics = data.clinics.filter(clinic => !clinics[clinic._id] || !clinics[clinic._id].action);
          const newStudents = data.students.filter(student => !students[student._id] || !students[student._id].action);
          total = newClinics.length + newStudents.length;
          if (!total) {
            dispatch({ type: GET_ALL });
          }
          newClinics.forEach(clinic => {
            clinics[clinic._id] = clinic;
            clinic.attachments.forEach(attachment => Image.prefetch(IMG_URL + attachment.data));
            if (++done === total) {
              SInfo.setItem('clinics', JSON.stringify(clinics), {});
              SInfo.setItem('students', JSON.stringify(students), {});
              dispatch({ type: GET_ALL });
            }
          });
          newStudents.forEach(student => {
            students[student._id] = student;
            SInfo.setItem('students', JSON.stringify(students), {});
            if (++done === total) {
              SInfo.setItem('clinics', JSON.stringify(clinics), {});
              SInfo.setItem('students', JSON.stringify(students), {});
              dispatch({ type: GET_ALL });
            }
          });
        });
      });
    })
    .catch(error => dispatch({ type: GET_ALL }));
  };
};

const postAllSuccess = (dispatch) => {
  dispatch({ type: GET_ALL });
};

const postAllFail = (dispatch, error) => {
  dispatch({ type: GET_ALL });
};

export const postAllCall = () => {
  return dispatch => {
    SInfo.getItem(USER, {}).then(value => {
      const doctor = JSON.parse(value);
      SInfo.getItem('clinics', {}).then(value => {
        const clinics = value !== null && value !== undefined ? JSON.parse(value) : {};
        SInfo.getItem('students', {}).then(value => {
          const students = value !== null && value !== undefined ? JSON.parse(value) : {};
          let count = 0;
          for (var key in clinics) {
            if (clinics[key].action) {
              ++count;
            }
          }
          for (var key in students) {
            if (students[key].action) {
              ++count;
            }
          }
          if (!count) return postAllSuccess(dispatch);
          let done = 0;
          for (var key in students) {
            var student = students[key];
            if (!student.action) continue;
            const {
              action,
              doctor,
              studentId,
              firstName,
              lastName,
              birthdate,
              mobile,
              landline,
              city,
              school,
              knownMedicalConditions,
              _id
            } = student;
            axios.post(action === 'edit' ? UPDATE_STUDENT_URL : CREATE_STUDENT_URL, {
              token: doctor.token,
              studentId,
              firstName,
              lastName,
              birthdate,
              mobile,
              landline,
              city: city._id,
              school: school._id,
              knownMedicalConditions,
              id: _id
            })
            .then(response => {
              if (!response.data.success) { 
                return postAllFail(dispatch, response.data.message); 
              }
              delete student.action;
              students[_id] = response.data.user;
              SInfo.setItem('students', JSON.stringify(students), {});
              if (++done === count) return postAllSuccess(dispatch);
            })
            .catch((error) => postAllFail(dispatch, error));
          }
          for (var key in clinics) {
            var clinic = clinics[key];
            if (!clinic.action) continue;
            const {
              action,
              doctor,
              student,
              speciality,
              complaint,
              history,
              findings,
              primaryDiagnosis,
              referral,
              referralReason,
              finalDiagnosis,
              attachments,
              _id
            } = clinic;
            axios.post(action === 'edit' ? UPDATE_CLINIC_URL : CREATE_CLINC_URL, {
              action,
              token: doctor.token,
              doctor: doctor._id,
              student: student._id,
              speciality: speciality ? speciality._id : null,
              complaint,
              history,
              findings,
              primaryDiagnosis,
              referral: referral ? referral._id : null,
              referralReason,
              finalDiagnosis,
              attachments: JSON.stringify(attachments),
              id: _id
            })
            .then(response => {
              if (!response.data.success) { 
                return postAllFail(dispatch, response.data.message); 
              }
              delete clinic.action;
              clinics[_id] = response.data.clinic;
              SInfo.setItem('clinics', JSON.stringify(clinics), {});
              if (++done === count) return postAllSuccess(dispatch);
            })
            .catch((error) => postAllFail(dispatch, error));
          }
        });
      });
    });
  };
}

export const getTitlesCall = () => {
  return dispatch => {
    NetInfo.isConnected.fetch().then(isConnected => {
      SInfo.getItem('titles', {}).then(value => {
        if (!isConnected && value !== null && value !== undefined) {
          return dispatch({ type: GET_ALL_TITLES, payload: JSON.parse(value) });
        } else {
          axios.get(ALL_TITLES_URL)
          .then(({ data }) => {
            SInfo.setItem('titles', JSON.stringify(data), {});
            dispatch({ type: GET_ALL_TITLES, payload: data });
          })
          .catch(error => Toast.show('Get titles failed'));
        }
      });
    });
  };
};

export const createDoctor = (title = '', firstName = '', lastName = '', speciality = '', email = '', password = '', mobile = '') => {
  return dispatch => {
    dispatch({ type: SIGNUP });
    axios.post(CREATE_DR_URL, { title, firstName, lastName, speciality, email, password, mobile })
    .then(({ data }) => {
      if (!data.success) {
        return loginFail(dispatch, data.message);
      }
      const user = data.user;
      user.token = data.token;
      dispatch(loginUserCall({ email, password }));
    })
    .catch(error => { loginFail(dispatch, 'Sign up failed'); });
  };
};

export const emailChangedCall = (text) => {
	return ({
		type: EMAIL_CHANGED,
		payload: text
	});
};

export const passwordChangedCall = (text) => {
	return ({
		type: PASSWORD_CHANGED,
		payload: text
	});
};

export const loginUserCall = ({ email, password }) => {
	return dispatch => {
		dispatch({ type: LOGIN_START });
	  axios.post(LOGIN_URL, { email, password })
    .then(({ data }) => {
      if (!data.success) { 
        return loginFail(dispatch, data.message); 
      }
      const { user, token } = data;
      user.token = token;
      logInUser(dispatch, user);
    })
    .catch(error => loginFail(dispatch, 'Sign in failed'));
  };
};

const loginFail = (dispatch, error) => {
  dispatch({ type: LOGINFAIL, payload: error });
  Toast.show(error);
};

const logInUser = (dispatch, user) => {
  dispatch({ type: LOGIN, payload: user });
  SInfo.setItem(USER, JSON.stringify(user), {});
  Keyboard.dismiss();
  Actions.main({ type: 'reset' });
};

export const logoutUserCall = () => {
  return dispatch => {
    Alert.alert(
      '',
      'Are you sure you want to logout?',
      [
        { text: 'No' },
        {
          text: 'Yes',
          onPress: () => {
            dispatch({ type: LOGOUT_USER });
            SInfo.deleteItem(USER, {});
            Actions.auth({ type: 'reset' });
          }
        }
      ],
      { cancelable: false }
    );
  };
};

