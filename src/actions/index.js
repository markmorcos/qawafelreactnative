import {
	getAllCall,
	postAllCall,
	emailChangedCall,
  passwordChangedCall,
  loginUserCall,
  createDoctor,
  logoutUserCall,
  getTitlesCall
} from './AuthActions';
import { 
	getCitiesCall, 
	getSchoolsCall, 
	createStudentCall, 
	readStudentCall
} from './StudentActions';
import { 
	propChangedCall,
	createClinicCall, 
	getSpecialityCall,
	getClinicsCall
} from './ClinicActions';

export const getAll = (cityId = '', schoolId = '') => {
	return getAllCall(cityId, schoolId);
};

export const postAll = () => {
	return postAllCall();
};

export const emailChanged = (text) => {
	return emailChangedCall(text);
};
export const propChanged = (key, value) => {
	return propChangedCall(key, value);
};
export const createClinic = (action, student, speciality, complaint, history, findings, primaryDiagnosis, referral, referralReason, finalDiagnosis, prescription, clinicId = '') => { 
	return createClinicCall(
		action,
		student,
		speciality,
		complaint,
		history,
		findings,
		primaryDiagnosis,
		referral,
		referralReason,
		finalDiagnosis,
		prescription,
		clinicId
	);
};
export const passwordChanged = (text) => {
	return passwordChangedCall(text);
};

export const getCities = () => {
	return getCitiesCall();
};

export const getSchools = () => {
	return getSchoolsCall();
};

export const createStudent = (action, sid, studentId, firstName, lastName, birthdate, mobile, landline, city, school, knownMedicalConditions) => {
	return createStudentCall(action, sid, studentId, firstName, lastName, birthdate, mobile, landline, city, school, knownMedicalConditions);
};

export const loginUser = ({ email, password }) => {
	return loginUserCall({ email, password });
};

export const getTitles = () => {
	return getTitlesCall();
};

export const signupUser = (title, firstName, lastName, speciality, email, password, mobile) => {
	return createDoctor(title, firstName, lastName, speciality, email, password, mobile);
};

export const logoutUser = () => {
	return logoutUserCall();
};

export const getSpeciality = () => {
return getSpecialityCall();
};

export const readStudent = (city, school, id) => {
	return readStudentCall(city, school, id);
};

export const getPrimaryClinics = (id) => {
	return getClinicsCall(id, 'primary');
};

export const getFinalClinics = (id) => {
	return getClinicsCall(id, 'final');
};
