 import {
	CLINIC_PROPS_CHANGED,
	CREATE_CLINIC,
	CREATE_CLINIC_SUCCESS,
	CREATE_CLINIC_FAIL,
	GET_ALL_SPECIALITY,
	GET_ALL_REFERRAL,
	GET_PRIMARY_CLINICS,
	GET_FINAL_CLINICS
} from '../actions/types';

const INITIAL_STATE = {
	speciality: '',
	doctor: '',
	student: '',
	complaint: '',
	history: '',
	findings: '',
	primaryDiagnosis: '',
	referral: '',
	referralReason: '',
	finalDiagnosis: '',
	attachments: [],
	originalAttachments: [],
	referrals: [],
	specialities: [],
	primaryClinics: [],
	finalClinics: [],
	loading: false,
	error: ''
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case CLINIC_PROPS_CHANGED:
			return { ...state, [action.payload.key]: action.payload.value };
		case CREATE_CLINIC:
			return { ...state, loading: true, error: '' };
		case CREATE_CLINIC_SUCCESS:
			return { ...state, loading: false };
		case CREATE_CLINIC_FAIL:
			return { ...state, error: action.payload, loading: false };
		case GET_ALL_REFERRAL:
			return { ...state, referrals: action.payload };
		case GET_ALL_SPECIALITY:
			return { ...state, specialities: action.payload };
		case GET_PRIMARY_CLINICS:
			return { ...state, primaryClinics: action.payload };
		case GET_FINAL_CLINICS:
			return { ...state, finalClinics: action.payload };
		default: return state;
	}
};
