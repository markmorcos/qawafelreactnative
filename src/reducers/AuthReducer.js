import {
	GET_ALL,
	EMAIL_CHANGED,
	PASSWORD_CHANGED, 
	LOGIN,
	LOGINFAIL,
	LOGIN_START,
	SIGNUP,
	LOGOUT_USER,
	GET_ALL_TITLES
} from '../actions/types';

const INITIAL_STATE = {
	email: '', 
	password: '',
	titles: [],
	user: null,
	error: '',
	loading: false,
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case GET_ALL:
			return { ...state, loading: false };
		case EMAIL_CHANGED:
			return { ...state, email: action.payload };
		case PASSWORD_CHANGED:
			return { ...state, password: action.payload };
		case LOGIN:
			return { ...state, ...INITIAL_STATE, user: action.payload };
		case LOGINFAIL:
			return { ...state, error: action.payload, password: '', loading: false };
		case LOGIN_START:
			return { ...state, loading: true, error: '' };
		case SIGNUP:
			return { ...state, loading: true, error: '' };
		case LOGOUT_USER:
    	return { ...state, ...INITIAL_STATE };
		case GET_ALL_TITLES:
			return { ...state, titles: action.payload };
		default:
			return state;
	}
};
