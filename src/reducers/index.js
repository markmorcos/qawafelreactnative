import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import MainReducer from './MainReducer';
import ClinicReducer from './ClinicReducer';

export default combineReducers({
	auth: AuthReducer,
	main: MainReducer,
	clinic: ClinicReducer 
});
