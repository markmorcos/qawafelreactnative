import {
	GET_ALL_CITIES,
	GET_ALL_SCHOOLS,
	CREATE_STUDENT,
	GET_FAILED,
	READ_STUDENT
} from '../actions/types';

const INITIAL_STATE = {
	cities: [],
	schools: [],
	student: null
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case CREATE_STUDENT:
			return { ...state };
		case GET_ALL_CITIES:
			return { ...state, cities: action.payload };
		case GET_ALL_SCHOOLS:
			return { ...state, schools: action.payload };
		case GET_FAILED:
			return { ...state };
		case READ_STUDENT:
			return { ...state, student: action.payload };
		default: return state;
	}
};
